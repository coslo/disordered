{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Dynamic correlation functions\n",
    "\n",
    "We will study the dynamics of a system composed of $N=8000$ particles under periodic boundary conditions and interacting via the Lennard-Jones potential\n",
    "$$\n",
    "u(r) = 4\\epsilon \\left[(\\frac{\\sigma}{r})^{12} - (\\frac{\\sigma}{r})^6\\right]\n",
    "$$\n",
    "which provides a good description of the properties of nobel gases (Ar, Xe, Ne). \n",
    "\n",
    "We will analyze trajectories obtained from molecular dynamics simulations performed in the microcanonical ensemble, at a density $\\rho=0.85$ and several temperatures. As usual, energies and lengths are measured in units of $\\epsilon$ and $\\sigma$, respectively. The following values provide a good description of the properties of Argon using the Lennard-Jones potential: $\\epsilon\\approx 120 K$ and $\\sigma \\approx 3.4\\times 10^{-10}m$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We download the trajectory files (in compressed xyz format) and store them in the current directory. They are bigger than the ones we used to study the structure, so be patient"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.core.utils import download\n",
    "\n",
    "download('https://moodle2.units.it/pluginfile.php/404689/mod_folder/content/0/lammps-T0.7.xyz.gz', '.')\n",
    "download('https://moodle2.units.it/pluginfile.php/404689/mod_folder/content/0/lammps-T4.0.xyz.gz', '.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us have a quick look at the thermodynamic state of the system"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from atooms.trajectory import TrajectoryXYZ\n",
    "\n",
    "with TrajectoryXYZ('lammps-T4.0.xyz.gz') as th:\n",
    "    system = th[0]\n",
    "    print(system)\n",
    "    print('T =', system.temperature)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The times at which the frames have been stored are spaced exponentially in time. This is a trick to cover both short and long time scales without occupying too much disk space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "plt.plot(th.times, 'o')\n",
    "th.times[:10]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, let's use some `postprocessing` tools to compute some dynamic correlation functions and plot them"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import atooms.postprocessing as pp\n",
    "\n",
    "with TrajectoryXYZ('lammps-T4.0.xyz.gz') as th:\n",
    "    cfh = pp.MeanSquareDisplacement(th)\n",
    "    cfh.compute()\n",
    "    \n",
    "with TrajectoryXYZ('lammps-T0.7.xyz.gz') as th:\n",
    "    cfl = pp.MeanSquareDisplacement(th)\n",
    "    cfl.compute()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "plt.plot(cfh.grid, cfh.value, '-o', label='T=4.0')\n",
    "plt.plot(cfl.grid, cfl.value, '-o', label='T=0.7')\n",
    "plt.xlabel('t')\n",
    "plt.ylabel('MSD')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with TrajectoryXYZ('lammps-T4.0.xyz.gz') as th:\n",
    "    cfh = pp.VelocityAutocorrelation(th, tgrid=th.times[:18])\n",
    "    cfh.compute()   \n",
    "with TrajectoryXYZ('lammps-T0.7.xyz.gz') as th:\n",
    "    cfl = pp.VelocityAutocorrelation(th, tgrid=th.times[:18])\n",
    "    cfl.compute()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(cfh.grid, cfh.value / cfh.value[0], '-o', label='T=4.0')\n",
    "plt.plot(cfl.grid, cfl.value / cfl.value[0], '-o', label='T=0.7')\n",
    "plt.xlabel('t')\n",
    "plt.ylabel('VACF')\n",
    "plt.legend()\n",
    "plt.grid()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Show the correlation using a log scale on the time axis: the time grid is exponentially spaced so that the the data points are evenly distributed (in time) when plotted in log scale."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import atooms.postprocessing as pp\n",
    "\n",
    "# Close to the first peak of S(k)\n",
    "kvectors = [7.0]\n",
    "\n",
    "# Note: cf.grid is a list containing the list of wave-vectors\n",
    "# as first entry and the list of times as second entry \n",
    "# cf.value is now a list of the correlation functions at fixed k\n",
    "\n",
    "with TrajectoryXYZ('lammps-T4.0.xyz.gz') as th:\n",
    "    cf1 = pp.SelfIntermediateScattering(th, kgrid=kvectors, tsamples=120)\n",
    "    cf1.compute()\n",
    "    \n",
    "with TrajectoryXYZ('lammps-T0.7.xyz.gz') as th:\n",
    "    cf2 = pp.SelfIntermediateScattering(th, kgrid=kvectors, tsamples=120)\n",
    "    cf2.compute()    "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "\n",
    "# We plot the F_s(k,t) for k=7 (the 6th entry of the time grid)\n",
    "plt.plot(cf1.grid[1], cf1.value[0], '-o', label='T=4.0')    \n",
    "plt.plot(cf2.grid[1], cf2.value[0], '-o', label='T=0.7')\n",
    "plt.xlabel('t')\n",
    "plt.ylabel('$F_s(k,t)$')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.semilogx(cf1.grid[1], cf1.value[0], '-o')\n",
    "plt.semilogx(cf2.grid[1], cf2.value[0], '-o')\n",
    "plt.xlabel('t')\n",
    "plt.ylabel('$F_s(k,t)$')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 1**: *velocity correlations*\n",
    "    \n",
    "1) Check that the value of the unnormalized velocity correlation function at $t=0$ is consistent with an equilibrium state.\n",
    "\n",
    "2) Focus on the initial decay of the velocity autocorrelation function $Z(t)$. Fit the normalized function to the form $1-\\frac{\\Omega^2}{2}t$ and obtain a rough estimate of $\\Omega$ (the so-called Einstein frequency), which gives the frequency at which a particle would oscillate around its position if the neighboring particles were frozen. How does it depend on temperature? Can you interpret this result? (you may want to have a look at the exercise 3.1 on the moodle page of the course).\n",
    "\n",
    "3) The function $Z(t)$ decays to zero within a rather short time span. Estimate this time scale using physical units appropriate for Argon (you may need the mass of a argon atom)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 2**: *Free particle regime*\n",
    "\n",
    "Compare the exact expressions for $F_s(k,t)$ and $F(k,t)$ in free particle regime against the simulation data\n",
    "- at fixed $k=7$ as a function of $t$\n",
    "- at fixed $t=0.012$ as a function of $k$\n",
    " \n",
    "*Hint*: use a log scale of the time axis for better visualization. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercise 3**: *Gaussian approximation*\n",
    "\n",
    "1) Test the Gaussian approximation\n",
    "$$\n",
    "F_s(k,t) = \\exp{\\left(-\\frac{1}{6} k^2 \\langle\\delta r^2(t)\\rangle\\right)}\n",
    "$$\n",
    "by superposing the actual self intermediate scattering function to its approximation for selected values of the wave-vector $k$. *Note*: make sure you use the same time grid for both $F_s(k,t)$ and $\\langle\\delta r^2(t)\\rangle$, by passing the same list of times to the `tgrid` parameter of each constructor.\n",
    "\n",
    "2) Repeat the test for the lowest acceptable wave-vector, which can be selected by asking for a negative wave-vector (ex. -1) in  `kgrid`"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  },
  "name": "dynamics.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
