{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Brownian dynamics\n",
    "\n",
    "We simulate the overdamped Brownian dynamics\n",
    "$$\n",
    "\\xi \\frac{d\\vec{r}}{dt} = \\vec{\\theta}(t) + \\vec{F}(\\vec{r})\n",
    "$$\n",
    "of a colloidal particle under the influence of an external force $\\vec{F}$ and compute its mean square displacement $\\langle |\\Delta \\vec{r}(t)|^2\\rangle$. As usual, several non-interacting particles are simulated in parallel to improve the statistics. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "import matplotlib.pyplot as plt "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Model parameters\n",
    "npart = 1000\n",
    "ndims = 2\n",
    "friction = 1.0\n",
    "k = 0.5\n",
    "T = 1.0\n",
    "dt = 0.04\n",
    "nsteps = int(3.0 / dt)\n",
    "\n",
    "# Fluctuation dissipation relation\n",
    "D = T / friction\n",
    "width = (2 * D * dt)**0.5\n",
    "\n",
    "# Overdamped Brownian dynamics\n",
    "positions = numpy.zeros((npart, ndims))\n",
    "msd = []\n",
    "x, y = [], []\n",
    "for i in range(nsteps):\n",
    "    # Store MSD\n",
    "    msd.append(numpy.sum(positions**2) / npart)\n",
    "    x.append(positions[0, 0])\n",
    "    y.append(positions[0, 1])\n",
    "    # Integration step\n",
    "    for position in positions:\n",
    "        noise = numpy.random.normal(0.0, width, ndims)\n",
    "        # No external field\n",
    "        force = numpy.zeros(ndims)\n",
    "        # Harmonic potential\n",
    "        # force = - k * position\n",
    "        position += noise + force * dt / friction\n",
    "\n",
    "time = numpy.array(range(nsteps)) * dt\n",
    "plt.plot(time, msd, 'o', label='Simulation')\n",
    "# Theoretical diffusive behavior (in the absence of external force)\n",
    "plt.plot(time, 2 * ndims * D * time, '-', label='Theory')\n",
    "# This is to plot an horizontal line at height y\n",
    "# plt.axhline(y, xmin=time[0], xmax=time[-1])\n",
    "plt.xlabel('t')\n",
    "plt.ylabel('MSD')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(x, y, '-o')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercice 1**: *drift-diffusion*\n",
    "\n",
    "The Brownian particles are subject to a constant external force $F>0$ in one dimension. This will lead to a systematic drift in the motion of the particles.\n",
    "\n",
    "1. Compute analytically the first two moments of the position, $\\langle x(t)\\rangle$ and $\\langle x^2(t)\\rangle$, and provide a physical interpretation of the results\n",
    "2. Cross-check the results against the simulation data and plot the results in a double logarithmic scale. Identify the ballistic and the diffusive regime."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercice 2**: *harmonic potential* \n",
    "\n",
    "Put the Brownian particles in a harmonic potential $U(x) = \\frac{1}{2}kx^2$ in one dimension.\n",
    "\n",
    "1. Check that $\\langle |\\Delta x(t)|^2\\rangle$ is diffusive in the absence of external field but saturates to a finite value when the harmonic potential is switched on\n",
    "\n",
    "2. Determine analytically the equilibrium value $\\langle |\\Delta x(t=\\infty)|^2\\rangle$ and compare it to the simulation data. Which factors control the relaxation time that describes the approach to equilibrium? "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercice 3**: *thermally activated barrier crossing*\n",
    "\n",
    "Study numerically the dynamics of a Brownian particle in a one-dimensional double well potential \n",
    "$$\n",
    "U(x) = U_0 (1 - (x/a)^2)^2\n",
    "$$\n",
    "The initial position of the particle will be at the bottom of the left well: $x(t=0)=-a$. \n",
    "1. Choose a temperature $T > U_0$, for which barrier crossing is easy, and plot a single trajectory $x(t)$ over a sufficiently long time. What do you observe?\n",
    "2. Reduce the temperature, $T < U_0$, and repeat the numerical experiment. What do you observe? \n",
    "3. Give the expression of the probability density $p(x)$ at equilibrium. Can you estimate the time needed to reach equilibrium? (simulate several independent particles in parallel)\n",
    "4. *[Hard]* Compute the exit time $\\tau$ of a particle initially confined in the left well, as a function of $T$. Compare your results with the Arrhenius law $\\tau = \\tau_0 \\exp(\\Delta U / k_BT)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Double well potential\n",
    "x = numpy.linspace(-1.5, 1.5, 100)\n",
    "plt.plot(x, (1-x**2)**2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Model parameters\n",
    "npart = 1\n",
    "ndims = 1\n",
    "friction = 1.0\n",
    "k = 0.5\n",
    "T = 0.1\n",
    "dt = 0.01\n",
    "nsteps = int(100.0 / dt)\n",
    "D = T / friction\n",
    "width = (2 * D * dt)**0.5\n",
    "\n",
    "# Overdamped Brownian dynamics\n",
    "positions = numpy.zeros((npart, ndims))\n",
    "pos = []\n",
    "for i in range(nsteps):\n",
    "    for position in positions:\n",
    "        noise = numpy.random.normal(0.0, width, ndims)\n",
    "        # force = numpy.zeros(ndims)\n",
    "        force = 4 * position * (1 - position**2)\n",
    "        position += noise + force * dt / friction\n",
    "    pos.append(position[0])   #numpy.sum(positions**2) / npart)\n",
    "\n",
    "time = numpy.array(range(nsteps)) * dt\n",
    "plt.plot(time, pos, '-', label='Simulation')\n",
    "plt.xlabel('t')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Exercice 4**: *stochastic resonance*\n",
    "\n",
    "Consider a Brownian particle in a double well potential\n",
    "$$\n",
    "U(x) = U_0 (1 - (x/a)^2)^2\n",
    "$$\n",
    "subject to an additional periodic force of the form\n",
    "$$\n",
    "F(x) = F_0 \\sin(2\\pi t/\\tau)\n",
    "$$\n",
    "where $A$ is the forcing amplitude and $\\tau$ is the forcing period. For convenience, set $U_0=1$ and $a=1$.\n",
    "\n",
    "1. Use Kramers theory to estimate the relaxation time $\\tau_K$ at $T=0.4$ *in the absence of forcing* ($A=0$).\n",
    "2. Add a small forcing $F_0=10^{-1}$ and plot the trajectory of the Brownian particle $x(t)$ over a time much longer than $\\tau_K$ in the following two cases: (i) $\\tau > \\tau_K$, (ii) $\\tau < \\tau_K$. What do you observe? \n",
    "3. Compare your results to the analysis of Benzi, Nonlin. Processes Geophys. 17, 431 (2010) (https://npg.copernicus.org/articles/17/431/2010/) and of Simon & Libchaber, Phys. Rev. Lett. 68, 3375 (1992) (https://journals.aps.org/prl/abstract/10.1103/PhysRevLett.68.3375)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "import matplotlib.pyplot as plt \n",
    "\n",
    "# Model parameters\n",
    "npart = 1\n",
    "ndims = 1\n",
    "friction = 1.0\n",
    "k = 0.5\n",
    "T = 0.4\n",
    "dt = 0.04\n",
    "nsteps = int(2000.0 / dt)\n",
    "U_0 = 1\n",
    "a = 1\n",
    "A = 1e-1\n",
    "tau = 100.0\n",
    "\n",
    "# Fluctuation dissipation relation\n",
    "D = T / friction\n",
    "width = (2 * D * dt)**0.5\n",
    "\n",
    "# Overdamped Brownian dynamics\n",
    "positions = numpy.zeros((npart, ndims))\n",
    "x = []\n",
    "for i in range(nsteps):\n",
    "    # Store MSD\n",
    "    x.append(positions[0, 0])\n",
    "\n",
    "    # Integration step\n",
    "    for position in positions:\n",
    "        noise = numpy.random.normal(0.0, width, ndims)\n",
    "        # No external field\n",
    "        force = numpy.zeros(ndims)\n",
    "        # Harmonic potential\n",
    "        force = 4 * U_0 / a**2 * position * (1-(position/a)**2)\n",
    "        force += A * numpy.sin(2*numpy.pi * i*dt / tau)\n",
    "        position += noise + force * dt / friction\n",
    "\n",
    "time = numpy.array(range(nsteps)) * dt\n",
    "plt.plot(time, x)\n",
    "# This is to plot an horizontal line at height y\n",
    "# plt.axhline(y, xmin=time[0], xmax=time[-1])\n",
    "plt.xlabel('t')"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
